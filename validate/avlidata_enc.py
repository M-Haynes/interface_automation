#!/usr/bin/env ptthon
# _*_ coding: utf-8 _*_
# @Author: zzd
# Time: 2024-4-22

import jsonpath
import requests
from jsonschema.validators import validate

from utils.jsonschema_tool import python_dict_to_json_schema
from validate.apidoc_examples import examlpes

# 此文件封装返回数据格式及类型断言校验

class ToolUtils(object):

    # 封装json_schema 断言
    def __schema_validate(self, key: str, response: requests.models.Response):
        response_example = examlpes[key]
        except_schema = python_dict_to_json_schema(response_example)
        actual_schema = python_dict_to_json_schema(response.json())
        try:
            validate(instance= response.json(), schema= except_schema)
            msg = "jsonschema断言[返回体格式]：校验通过\n"
            print(f'{msg}')
        except Exception as e:
            message = f'''
            jsonschema断言[返回体数据格式]校验失败：\n
            接口文档示例展示数据格式为：{except_schema}，
            实际返回体数据格式为：{actual_schema}\n
            异常信息：{e}\n
            '''
            assert False, message


    # 封装json断言
    def __jsonpath_validate(self, validate_datas: [], response):
        res = response.json()
        for val_data in validate_datas: # 遍历循环，实现支持多重断言
            actual_value_list = jsonpath.jsonpath(res, val_data['jsonpath'])

            # 目前是只分装了包含指定字符串的断言，后续有空还会继续封装
            if actual_value_list:
                actual_value = actual_value_list[0]
                try:
                    assert val_data.get('expect_data') in actual_value
                    msg = f'jsonpath断言[{val_data.get("assert_desc")}]: 校验通过'
                    print(msg)

                except Exception as e:
                    message = f'''
                    jsonpath 断言[{val_data.get('assert_desc')}]: 校验失败\n;
                    期望值为：{val_data.get('expect_value')}\n;
                    实际值为：{actual_value}\n
                    '''
                    assert False,message
            else:
                msg = "jsonpath 表达式书写错误！ 请重新检查\n"
                raise msg


    def __text_validate(self,validate_dates: [] , response):
        # text 断言书写内容部位空，防止就写一个text： 后面什么都没有
        if validate_dates:
            res = response.text # 获取响应文本
            # 遍历循环，支持多重断言
            for val_data in validate_dates:
                try:
                    assert val_data.get('expect_value') in res
                    msg = f'text断言[{val_data.get("assert_desc")}]:校验通过\n'
                    print(msg)

                except Exception as e:
                    message = f'''
                    text断言[{val_data.get('assert_desc')}]: 校验失败\n;
                    期望值为：{val_data.get('expect_value')}\n;
                    实际值为：{res}\n
                    '''
                    assert False, message

    # 封装成函数进行调用
    def assert_func(self, validate_data, response):
        for validate_method in validate_data:

            # 用户使用的是jsonschema断言
            if validate_method == 'jsonschema':
                self.__schema_validate(validate_data.get(validate_method), response)

            # 用户使用的是jsonpath断言
            if validate_method == 'jsonpath':
                self.__jsonpath_validate(validate_data.get(validate_method), response)

            # 用户使用的文本断言
            if validate_method == 'text':
                self.__text_validate(validate_data.get(validate_method), response)


toolutils = ToolUtils()
