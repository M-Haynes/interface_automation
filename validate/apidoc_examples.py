#!/usr/bin/env python
# _*_ coding: utf-8
# @Author: zzd
# @Time: 2024-4-22

# 此处存储接口文档给的 每个接口的响应体示例
# 只需要复制然后粘贴到整个examples字典即可，给粘贴的内容设置一个字典的键，便于寻找
examlpes = {
    # --------------- JWT 登录接口 --------------------
    # 1 成功场景
    # 响应结果示例：字典的键自己取，见名之意，需每个键加上注释
    "JWT_login_success": {
        "code": "success",
        "msg": None,
        "data": {
            "jwt": "xxxxx",
            "lan": "zh-CN"
        }
    },

    # 2. 登录失败场景 -- 用户名或密码错误
    "JWT_login_fail_error_password": {
        "code": 'E000002',
        "msg": '用户名或密码错误。如密码错误达到10次，账号将被锁定1小时。',
        "data": None,
        "page": None
    },

    # 3. 登录失败场景 -- 公司名称错误
    "JWT_login_fail_error_comp": {
        "code": "E000002",
        "msg": "公司 xxx 没有被找到! 请检查您输入的公司名。",
        "data": None,
        "page": None},



    # ---------------------- 登出接口 -----------------------------
    # 1 登出成功场景
    "loginOut_success":{
        "code": "success",
        "msg": None,
        "data": None,
        "page": None
    },
}