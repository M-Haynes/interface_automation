"""全局 fixture、hook函数"""
#!usr/bin/env python
# _*_ coding: utf-8 _*_
# @Author: zzd
# @Time：2024-4-10

import os
from datetime import datetime

# 动态生成log文件的名称，哪怕配置文件中配置了log_file = ./log/test 也不会生效
def pytest_configure(config):
    time_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    config.option.log_file = os.path.join(config.rootdir, 'log', f'{time_now}')

