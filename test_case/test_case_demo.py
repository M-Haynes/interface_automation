#!/usr/bin/env python
# _*_ coding: utf-8 _*_
# @Author: zzd
# @Time: 2024-4-12

import pytest
import logging
import allure
logger = logging.getLogger(__name__)
from conf.settings import CASE_DATA_PATH
from utils.yaml_tool import read_data_from_yaml
from utils.common_var_tool import cv
from utils.substitution_tool import var_substitution, parameter_substitution
from utils.http_requests import req
from utils.mysql_tool import MySQLDB
from utils.assert_tool import assert_res
from conf.settings import ENV_HOST_PORT
from utils.global_variable_tool import gv


"""标记测试用例的特征、故事和严重级别"""
@allure.feature('Example')
@allure.story('Test Case')
@allure.severity(allure.severity_level.BLOCKER)
class TestCase:
    """使用pytest的parametrize装饰器来传递测试数据"""
    @pytest.mark.parametrize("test_data", read_data_from_yaml(CASE_DATA_PATH))
    def test_case(self, test_data):
        common_data = cv.get_common_vars()
        logger.info(f"--当前测试的常用变量是:%s" % common_data)

        # 获取initSql
        initSql = test_data["initSql"]
        # 类型是list
        logger.info(type[initSql])
        headers = test_data["request"]["headers"]
        logger.info(">>>> headers:{}".format(headers))

        # request_url = ENV_HOST_PORT + test_data["request"]["url"]
        request_url = test_data["request"]["url"]  # 请求的url，待拼接
        logger.info(">>>> request_url:{}".format(request_url))

        params = test_data["request"]["params"]
        logger.info(">>>> params:{}".format(params))

        method = test_data["request"]["method"]
        logger.info(">>>> method:{}".format(method))

        cookies = test_data['request']['cookies']
        logger.info(">>>> cookies:{}".format(cookies))

        # 替换变量值为实际值
        if params:
            # 依赖值替换为实际值
            params = parameter_substitution(str(params))
            # 常用变量占位符替换为实际值
            params = var_substitution(params)
            logger.info(">>>> 替换后params：{}".format(params))

        # 调佣http_requests.py文件中的请求函数
        response = req.send_request(request_url, method, headers, params)




