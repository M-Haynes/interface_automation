#!/usr/bin/env python
# _*_coding: utf-8_*_
# @Author: zzd
# @time: 2024-4-10

import pytest
import logging
logger = logging.getLogger(__name__)
from conf.settings import CASE_DATA_PATH
from utils.yaml_tool import read_data_from_yaml
from utils.substitution_tool import var_substitution, parameter_substitution
from utils.mysql_tool import MySQLDB
from conf.settings import ENV_HOST_PORT
my = MySQLDB.get_instance()

'''
class TestCase:
    @pytest.mark.parametrize("casedata", read_data_from_yaml(CASE_DATA_PATH))
    def test_case(self, casedata):
        # logger.info(f"---当前测试用例数据是：{casedata}")
        # assert 1 == 1
        var_substitution()
'''

'''
# 这个类是实现常用变量的获取
class TestCase:
    @pytest.mark.parametrize("casedata", read_data_from_yaml(CASE_DATA_PATH))
    def test_case(self, casedata):
        # logger.info(f"---当前测试用例数据是：{casedata}")
        var_substitution()
'''

'''
""" 优化： 这个类只一次性获取常量，而不在需要每次都进行获取"""
class TestCase:
    @pytest.mark.parametrize("casedata",read_data_from_yaml(CASE_DATA_PATH))
    def test_case(self, casedata):
        common_vars = CommonVars()
'''


'''
"""这一步是链接mysql数据库，并对数据库中的数据进行解析"""
class TestCase:
    @pytest.mark.parametrize("casedata", read_data_from_yaml(CASE_DATA_PATH))
    def test_case(self, casedata):
        # logger.info(f"---当前测试用例数据是：{casedata}")

        # 获取initSql
        initSql = casedata["initSql"]
        # 类型是list
        # logger.info(type(initSql))
        # logger.info(">>>>>initSql:{}".format(initSql))
        if initSql:
            with my.get_instance().conn.cursor() as cursor:
                for sql in initSql:
                    logger.info("==================替换前sql:{}".format(sql))
                    sql = var_substitution(sql)
                    logger.info(f"==================替换后sql:{sql}")
                    if sql.strip().startswith('select'):
                        res = cursor.execute(sql)
                        logger.info("{}查询结果是：{}".format(sql, res))
                    else:
                        res = cursor.execute(sql)
                        logger.info("操作成功：{}条".format(res))
        else:
            logger.error("测试数据中缺少 'initSql' 键")
'''

class TestCase:
    @pytest.mark.parametrize("casedata", read_data_from_yaml(CASE_DATA_PATH))
    def test_case(self, casedata):
        # logger.info(f"---当前测试用例数据是：{casedata}")

        # 获取initSql
        initSql = casedata["initSql"]
        # 类型是list
        # logger.info(type(initSql))
        # logger.info(">>>>>initSql:{}".format(initSql))
        headers = casedata["request"]["headers"]
        logger.info(">>>>> headers：{}".format(headers))
        # logger.info(">>>>> heasers:%s" % headers)

        url = ENV_HOST_PORT + casedata["request"]["url"]
        logger.info(">>>> url:{}".format(url))

        params = casedata["request"]["params"]
        logger.info(">>>> 替换前params：{}".format(params))

        method = casedata['request']['method']
        logger.info(">>>> method：%s" %method)

        if params:
            params = parameter_substitution(str(params))
            params = var_substitution(params)
        logger.info(">>>>> 替换后params：{}".format(params))

        # 调佣http_requests.py文件中的请求函数，实现请求

