#!/usr/bin env python
# _*_ coding: utf-8 _*_
# @Author: zzd
# @Time: 2024-4-16

"""实现通过钉钉机器人，发送消息"""
import time, hmac, hashlib, base64
import urllib.parse
from dingtalkchatbot.chatbot import DingtalkChatbot
from conf.settings import webhook, secret
from utils.make_text import make_text

def dd_send_msg(text, webhook = webhook, secret = secret):
    # 加签
    timestamp = str(round(time.time()*1000))
    secret_enc = secret.encode('utf-8')  # 将secret字符串编码为UTF-8格式的字节串
    string_to_sign = '{}\n{}'.format(timestamp, secret)  # 将时间戳和密钥拼接成一个待签名的字符串
    string_to_sign_enc = string_to_sign.encode('utf-8')
    hmac_code = hmac.new(secret_enc, string_to_sign_enc, digestmod=hashlib.sha256).digest()
    sign = urllib.parse.quote_plus(base64.b64encode(hmac_code))
    webhook = webhook + '&timestamp=' + timestamp + '&sign='+sign

    # 实例化机器人
    ddrobot = DingtalkChatbot(webhook)

    # 调用机器人的发送消息功能
    ddrobot.send_text(msg=text)

if __name__ == '__main__':
    # 钉钉消息模版配置
    text = make_text()[0]