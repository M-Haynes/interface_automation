""" 读取 yaml文件中的数据 """
#!/usr/bin/env python
# _*_ coding: utf-8 _*_
# @Author: zzd
# @Time: 2024-04-10


import yaml

# 读取yaml中用例/ 变量数据
def read_data_from_yaml(file_path):
    f = open(file_path, "r", encoding="utf-8")
    res = yaml.load(f, yaml.FullLoader)
    return res
