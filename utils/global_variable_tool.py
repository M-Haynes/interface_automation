# 作用： 用于获取依赖参数
#!/usr/bin/ env python
# _*_coding: utf-8 _*_
# @Author: zzd
# Time: 2024-4-11
import json
import logging
import jsonpath
from conf.settings import *
logger = logging.getLogger(__name__)


class GlobalVariables(object):
    '''单利对象， 保存依赖数据'''
    __instance = None

    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = object.__new__(cls, *args)
        return cls.__instance

    def __init__(self):
        self.globalVars = {"ip": ENV_HOST_PORT}
        self.res = []

    # 从yaml文件中读取全局变量，

    def setVar(self, key, value):
        '''添加全局变量'''
        self.globalVars[key] = value
        print("当前全局变量：",self.globalVars)

    def getVar(self, key):
        '''获取某个全局变量'''
        # 假设全局变量存储在一个字典self.globalVars中
        return self.globalVars.get(key)

    def getVars(self):
        '''获取全部全局变量'''
        return self.globalVars

    def deleteVar(self,key):
        '''删除某个全局变量'''
        self.globalVars.pop(key)

    def cleanVars(self):
        '''清空全局变量'''
        self.globalVars.clear()

    def deleteVars(self):
        '''删除全局变量'''
        del self.globalVars

    def save_global_variable(self, globalVariable, res):
        '''保存依赖数据到全局变量中'''
        for globalv in globalVariable.split(";"):
            g = globalv.strip()
            if g:
                key = g.split('=')[0].strip()
                value_expr = g.split('=')[1].strip()
                print("key:",key)
                print("value_expr:", value_expr)
                # 返回列表，区第一个值
                value = jsonpath.jsonpath(json.loads(res), value_expr)[0]

                self.setVar(key, value)
        self.getVar()  # 打印当前所有全局变量

gv = GlobalVariables()

