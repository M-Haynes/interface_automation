"""作用 封装发送http请求的功能"""
#!/usr/bin/env python
# _*_coding: utf-8 _*_
# @Author: zzd
# @Time: 2024-4-11

import requests
import logging
from utils.substitution_tool import parameter_substitution
logger = logging.getLogger(__name__)
from conf.settings import ENV_HOST_PORT

class HttpRequester(object):
    def __init__(self, ENV_HOST_PORT):
        self.ENV_HOST_PORT = ENV_HOST_PORT

    def send_request(self, request_url,  method, headers, params, data=None):
        """
        :param endpoint: The endpoint of the request (e.g: '/api/nologin/login')
        :param method: The HTTP method (e.g: 'GET', 'post')
        :param headers: A dictionary of headers to include in de request
        :param params: A dictionary of query parameter or json data for the request
        :param data: Optional  The data to be sent in a 'POST' request
        :return: The response object from the requests library
        """
        # # 将参数中的占位符替换为实际值
        params = parameter_substitution(str(params))

        # # 准备URL
        url = self.ENV_HOST_PORT + request_url

        # 初始化response为None
        response = None

        # 发送请求
        if method.upper() == 'GET':
            try:
                requests.DEFAULT_RETRIES = 5  # 这里设置遇到网络错误时的重试次数
                s = requests.session()  # 这里是允许我们跨请求保持某些参数，例如 cookies、headers
                s.keep_alive = False   # 这里是关闭默认的持久连接的功能
                response = requests.get(url, headers=headers, params=params, timeout=15)
                logger.info("执行请求后，结果是：%s" % response.text)

            except Exception as e:
                logger.error('出错了， 错误是%s'%e)

        elif method.upper() == 'post':
            try:
                if data:
                    response = requests.post(url, headers=headers, json=data)
                    logger.info("执行post请求后，结果是：%s"%response.text)
                else:
                    response = requests.patch(url, headers=headers, data=params)
                    logger.info("执行请求后，结果是：%s" % response.text)

            except Exception as e:
                logger.info("出错了，错误是：%s"%e)
        else:
            try:
                response = requests.request(method, url, headers=headers, json=params)
                logger.info("执行请求后，结果是：%s" % response.text)

            except Exception as e:
                logger.info("出错了，错误是：%s"%e)

        return response

req = HttpRequester(ENV_HOST_PORT)
