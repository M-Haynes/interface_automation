#!/usr/bin/env python
# _*_ coding: utf-8 _*_
# @Author：zzd
# @Time：2024-4-22

# 导入typing 库，用于类型提示
from typing import Dict, Union, List, Any, Literal

# 导入Enum类，用于定义枚举类型
from enum import Enum

# 定义一个名为JsonTypes 的枚举类，用于表示json支持的基本数据类型

class JsonTypes(Enum):
    STRING = 'string'  # 字符串类型
    INTEGER = 'integer'  # 整数类型
    NUMBER = 'number'  # 数值类型（包括整数和浮点数）
    BOOLEAN = 'boolean'  # 布尔类型
    ARRAY = 'array'  # 数组类型
    OBJECT = 'object'  # 对象类型(即字典类型)


# 定义一个函数identity_type,接收任意类型的对象作为参数
# 返回对应的JsonTypes 枚举成员
def indentity_type(obj:any) -> JsonTypes: # 使用 Python 类型注解的函数声明
    # 对象是字符串类型
    if isinstance(obj, str):
        return  JsonTypes.STRING

    # 对象是整数或浮点数类型
    elif isinstance(obj, (int, float)):
        if isinstance(obj, int):  # 如果整数
            return JsonTypes.INTEGER
        else:
            return JsonTypes.NUMBER

    # 对象是布尔类型
    elif isinstance(obj, bool):
        return JsonTypes.BOOLEAN

    # 对象是数组类型
    elif isinstance(obj, list):
        return JsonTypes.ARRAY

    # 对象是对象类型
    elif isinstance(obj, dict):
        return JsonTypes.OBJECT

    else:
        raise ValueError(f"Unsupported data type :{type(obj)}")


# 定义一个函数python_dict_to_json_schema
# 接收一个python字典，将其转换为 json schema结构
def python_dict_to_json_schema(python_dict) -> dict:
    schema = {"type":"object", "properties":{}}

    for key, value in python_dict.items():
        property_schema = {}
