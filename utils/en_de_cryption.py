#!/usr/bin/env python
# _*_ coding: utf-8 _*_
# @Author: zzd
# @Time: 2024-4-19

import base64

def bse64_encode(data):
    """
    用于对系统数据进行base64进行加密处理
    :param data:  原始入参字符串
    :return:  base64加密完成后的字符串
    """
    byte_data = data.encode('utf-8')
    encoded_data = base64.b64decode(byte_data)
    return encoded_data.decode('utf-8')

def  base64_decode(data):
    """
    用于对数据系统数据进行base64解密处理
    :param data:  接收到的加密字符串
    :return:  解密后的字符串
    """
    encode_data = data.encode('utf-8')
    decode_data = base64.b64decode(encode_data)
    return decode_data.decode('utf-8')

