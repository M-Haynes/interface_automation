"""添加一个工具模块， 这个模块将负责加载和缓存常用变量
    以便其他模块可以直接访问这些变量，而无需再次读取文件"""
#!/usr/bin/env python
#_*_coding: utf-8 _*_
# @Author: zzd

import os
from utils.yaml_tool import read_data_from_yaml
from conf.settings import VAR_DATA_PATH
import logging

logger = logging.getLogger(__name__)

class CommonVars(object):
    _instance = None
    _vars = None  # 用于存储加载的常用变量数据

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(CommonVars, cls).__new__(cls)
            cls._instance._load_vars()
        return cls._instance

    def _load_vars(self):
        self._vars = read_data_from_yaml(VAR_DATA_PATH)
        logger.info("常用变量已加载：{}".format(self._vars))

    def __getitem__(self, key):
        return self._vars.get(key)

    @classmethod
    def get_common_vars(cls):
        if cls._instance is None:
            cls._instance = CommonVars()  # 确保实例被创建
        return cls._instance._vars

# 创建一个公共变量实例并导出
cv = CommonVars()

