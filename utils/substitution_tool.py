#!/usr/bin/env python
#_*_coding: utf-8_*_
# @Author: zzd
# @Time:2024-04-10
import re

from utils.yaml_tool import read_data_from_yaml
from conf.settings import VAR_DATA_PATH
import yaml
import base64
import pytest
from conf.settings import PATTERN
from utils.common_var_tool import CommonVars as cv
from utils.global_variable_tool import gv
import logging
logger = logging.getLogger(__name__)



'''
def var_substitution():
    """先获取常用变量进行占位，后续替换成实际值"""
    var_dic = read_data_from_yaml(VAR_DATA_PATH)
    logger.info("常用变量：{}".format(var_dic))
'''

"""
优化：优化读取逻辑，使得支持只读取依次常亮
"""
'''
def var_substitution():
    common_var = CommonVars()
    # 直接使用 common_var 获取变量值
    username = common_var['#{username}']
    password_correct = common_var['#{password_correct}']
    password_wrong = common_var['#{password_wrong}']
    product_name = common_var['#{product_name}']

    logger.info("常用变量已加载：{}".format(common_var.get_all()))
'''

"""添加替换变量工具模块"""
def var_substitution(param):
    ''' 常用变量占位符替换为实际值 '''
    var_dic = cv.get_common_vars()
    logger.info("常用变量：{}".format(var_dic))

    for key in var_dic:
        if key in param:
            param = param.replace(key, str(var_dic[key]))  # replace返回替换后的新字符串
    return param

"""添加替换依赖参数的方法"""
def parameter_substitution(param):
    ''' 依赖值替换为实际值 '''
    keys = re.findall(PATTERN, param)
    print(keys)
    print("#######################  分割符 ########################")
    for key in keys:
        value = gv.getVar(key)
        param = param.replace('${' + key + '}', str(value))  # replace 返回替换后的新字符串
    return param

