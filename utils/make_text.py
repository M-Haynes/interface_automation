#!/usr/bin/env python
#_*_ coding: utf_8 _*_
# @Author: zzd
# @Time: 2024-4-16

"""封住发送消息的模版 -- 这里主要是钉钉消息"""
import os, json
import logging
logger = logging.getLogger()
from pathlib import Path
from conf.settings import project_name, version, report_base_url


def make_text():
    '''
    构造钉钉发送的信息
    :return:
    '''
    history_data = {}
    report_name = ''
    reports_path = Path(__file__).parent.parent/'reports'  # 获取报告路径
    logger.debug(f"History data:{history_data}")
    logger.debug(f"reports_path：{reports_path}")

    for dirpath, dirname, filename in os.walk(reports_path):
        filename.sort(reverse=True)  # 将当前目录下所得的文件降序排序
        report_name = filename[1]  # 获取第2个文件，就是当前的测试报告文件
        history_data = filename[0]  # 获取第一个文件，就是当前的总的历史数据文件
        history_json_path = Path(reports_path) / history_data  # 获取历史文件完整路劲
        with open(history_json_path,'r', encoding='utf-8') as f:  # 读取历史文件中的数据
            history_data = json.load(f)[-1]  # 文件中最后一条为最近执行的测试用例情况
        break
    report_url = report_base_url + '/{}'.format(report_name)  # 组装测试报告访问链接
    logger.debug(f"report_url:{report_url}")
    # 构造钉钉信息内容
    dd_text = f'''{project_name}_{version} 接口自动巡查结果如下：\n
-----------------
开始执行时间：{history_data.get('begin_time')}
运行花费时间：{history_data.get('runtime')}\n
-----------------
用例执行总数：{history_data.get('all')}
用例执行通过数：{history_data.get('success')}
用例执行通过率：{history_data.get('pass_rate')}%\n
-----------------
用例执行失败数：{history_data.get('fail')}
用例执行错误数：{history_data.get('error')}
用例执行跳过数：{history_data.get('skip')}\n
-----------------
点击下面链接查看详细测试报告：
{report_url}
'''
    logger.debug(f"dd_text:{dd_text}")
    # 构造email信息内容：
    email_text = [
"-------------------------------",
f"开始执行时间:{history_data.get('begin_time')}",
f"运行花费时间：{history_data.get('runtime')}\n",
"-------------------------------",
f"用例执行总数：{history_data.get('all')}",
f"用例执行通过数：{history_data.get('success')}",
f"用例执行通过率：{history_data.get('pass_rate')}%\n",
"--------------------------------",
f"用例执行失败数：{history_data.get('fail')}",
f"用例执行错误数：{history_data.get('error')}",
f"用例执行跳过数：{history_data.get('skip')}\n",
"--------------------------------"]

    # 返回钉钉信息内容
    return dd_text, email_text

if __name__ == '__main__':
    print(make_text())
