#!/user/bin/env python
# _*_ coding: utf-8 _*_
# @Author： zzd
# @Time：2024-4-12

"""这个文件实现的是,实现接口请求的结果和预期结果是否一致
   后期优化可增加特殊的返回值进行校验
   封装成类后，便于后期优化"""
import jsonpath, json

class Assert(object):
    def assert_res(assertRes,res):
        """
        :param assertRes: 接口的期望参数
        :param response: 接口请求时响应的参数
        :return: res_status-- 接口参数返回的状态
        """
        res_status = 'pass'
        for i in assertRes.split(";"):
            i_ = i.strip()
            if i_:
                actual_expr = i_.split("=")[0].strip()
                actual = jsonpath.jsonpath(json.loads(res), actual_expr)[0]
                expect = i_.split("=")[1].strip()
                if str(actual) != expect:
                    res_status = 'fail'
                    return res_status

        return res_status

assert_res = Assert()