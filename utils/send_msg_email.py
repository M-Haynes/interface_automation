#!/usr/bin/env python
# _*_ coding: utf-8 _*_
# @Author: zzd
# @Time: 2024-4-18

from pathlib import Path
import smtplib
import os, shutil
from pathlib import Path
from email.mime.text import MIMEText
from conf.settings import nginx_reports_path
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from conf.settings import email_host, email_port, project_name,email_password, email_receivers,email_sender,email_info, version, report_base_url


def copy_report(locator_path=nginx_reports_path):
    """
    将最新的测试报告复制到指定的目录，
    以便于后续的查看或进一步处理，如发送邮件或上传到服务器等
    """
    path = None
    report_name = None
    reports_path = Path(__file__).parent.parent / 'reports'
    for dir_path, dir_name, filename in os.walk(reports_path):
        filename.sort(reverse=True)
        report_name = filename[1]

        path = dir_path + '/' + report_name  # 找到要发送的邮件的文件路径
        path = os.path.normpath(path)  # 规范化路径
        break

    file = path
    shutil.copy(file, locator_path)  # 将文件复制到目录
    return report_name # 将要送的报告名称返回


def send_email(email_text):
    """
    :param email_text: 传入参数 -- 要发送的
    :return:
    """
    host = email_host  # 邮件服务地址
    port = email_port  # 服务器端口号
    user = email_sender  # 代理邮箱
    password = email_password  # 代理邮箱的授权码

    report_name = copy_report()
    report_time = report_name.split('.')[0]

    # 发邮件的基本信息
    sender = user  # 发送人
    recevier = email_receivers  # 接受人，多个接受人放到同一个列表中
    subject = "{}_{}接口自动化{}巡检结果报告".format(project_name, version, report_time)

    report_url = report_base_url + '/'.format(report_name)

    connect_1 = f"""
    <h3>{project_name}_{version}接口巡检报告</h3>
    <p>{email_text[0]}<p>
    <p>{email_text[1]}<p>
    <p>{email_text[2]}<p>
    <p>{email_text[3]}<p>
    <p>{email_text[4]}<p>
    <p>{email_text[5]}<p>
    <p>{email_text[6]}<p>
    <p>{email_text[7]}<p>
    <p>{email_text[8]}<p>
    <p>{email_text[9]}<p>
    <p>{email_text[10]}<p>
    <p>{email_text[11]}<p>
    详细的测试报告访问路径为[请在公司内网访问]：<a href = {report_url} > {report_url} </a></br>
    <p>如有需要，可以通过附件下载本次测试报告</p>
    """  # 发送html的内容

    # 发送html测试报告的附件
    report_by_send = Path(__file__).parent.parent / 'reports' / '{}'.format(report_name)
    f = open(report_by_send, "rb")  # 发送html文件
    connect_2 = f.read()
    f.close()

    msg_1 = MIMEText(_text=connect_1, _subtype="html", _charset="utf-8")  # 添加文本内容

    # 添加待附件的内容
    msg_2 = MIMEApplication(connect_2)
    msg_2.add_header('content_disposition', 'attachment', filename='{}').format(report_name)

    message = MIMEMultipart()  # 申明邮件内容为 文本、附件等综合性内容
    message["From"] = sender  # 添加消息发送人
    message["To"] = ";".join(recevier)  # 添加消息收件人
    message["Subject"] = subject  # 添加消息的标题
    for msg in [msg_1, msg_2]:  # 添加正文内容
        message.attach(msg)

    # 发送邮件
    con = smtplib.SMTP_SSL(host, port)  # 指定邮件服务器地址及端口号，生成代理链接
    con.login(user, password)  # 登录代理链接
    con.sendmail(sender, recevier, message.as_string())  # 利用代理链接将内容发出去
    con.quit()  # 关闭代理链接















    """
    尊敬的刘女士
        今天没有呼唤你为亲爱的，想必是因为 这个特殊的时刻，需要一些认真的态度来表达我最真挚情感和想和你结婚的态度.
        我，周兆杜很感谢在四年遇见了你，很感谢你果断的和我相向而行，展露心意，很感激这四年，让我们从相爱到深爱，从相知到相伴，
    """
