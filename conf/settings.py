"""定义测试用例数据文件路劲"""

#!/usr/bin/env python
# _*_ coding: utf-8 _*_
# Author: zzd
# @Time: 1996-04-10

import os
import logging
import pymysql
from pathlib import Path
logger = logging.getLogger(__name__)

# 测试服务器地址路径  -- 暂不使用该路径
# base_url = 'http://192.168.1.5:5000/pm/11Index'
# base_url = 'https://pre.leiga.net'

#获取项目路径
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# 定义测试用例数据文件
CASE_DATA_PATH = os.path.join(BASE_PATH, 'data/test_case.yaml')

# 定义常用变量数据文件路径 -- 常用变量即最基础的变量
VAR_DATA_PATH = os.path.join(BASE_PATH, 'data/variable.yaml')

# 环境信息 -- 用于拼接成完成的接口
ENV_HOST_PORT = 'https://pre.leiga.net'

# 替换符
PATTERN = '${(.*?)}'

mysql_info = {
    "host": "192.168.1.203",
    "port": 3306,
    "user": "zzd_1",
    "password": "123456",
    "db": "zzd_jiekou",
    "charset": "utf8"
}
# # 定义数据库连接
# my = pymysql.connect(**mysql_info)


# 钉钉消息发送，webhook地址及secret值
webhook = 'https://oapi.dingtalk.com/robot/send?access_token=348b332108b0d2f1437852db7e63ddcdbfaea6b3ed9bee49d4c006f90a4f9676'
secret = 'SEC83630f9162521d2c7a7874b98de70bd20ad3e61e1892c43f36fb3177d04b2090'


# nginx服务器访问的测试报告路径
# nginx_reports_path = '/usr/share/nginx/html/reports/xxx'  #到时候服务器配置
nginx_reports_path = Path(__file__).parent.parent / 'cp_reports'  #本地代码调试使用

#邮件发送配置
email_host = "xxx.163.com" #邮箱服务器地址
email_port = 465 # 邮箱服务器端口号
email_info = ['easytrack888@163.com','RSNFPVVIUVYEHYJB']
email_sender = email_info[0]  # 邮件发送人
email_password = email_info[1]  # 邮箱授权码
email_receivers = [
    "zhouzhaodu@etppm.com"
] # 邮件接受人,所有的邮件接收人，均放在列表中

# 项目名称及版本
project_name = 'Easytrack_zzd'
version = 'v1.4_zzd'

# 项目测试报告访问的基本路径
report_base_url = 'http://192.168.1.203:1123/reports/easytrack'

# 测试人员
tester = 'zzd'
