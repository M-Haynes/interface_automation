"""主运行文件"""
#!/usr/bin/env python
# _*_ coding: utf-8 _*_
# @Author: zzd
# @Time: 2024-4-10

import os
import time
import subprocess
import logging
from utils.send_msg_dd import dd_send_msg
from utils.make_text import make_text
from conf.settings import project_name, version, tester

"""
 没有集成到allure报告，以及发送钉钉消息
if __name__ == '__main__':
    pytest.main()
"""

# 定义报告的名称和目录
report_name = time.strftime('%Y%m%d%H%M%S') + 'allure_reports'
allure_report_dir = f"./reports/allure-results-{report_name}"
html_report = f"./reports/{report_name}.html"


if __name__ == '__main__':
    logging.basicConfig(level= logging.INFO)
    logger = logging.getLogger()
    logger.info("Logger is configured!!")  # 这条日志用于测试日志是否工作

    # 记录日志信息
    logger.info("Starting the test suite...")

    # 执行pytest命令测试，生成allure报告
    subprocess.run(["pytest"])

    # 收集allure报告的链接
    allure_report_link = os.path.join(os.getcwd(),'reports', report_name, 'index.html')

    # 生成文本消息
    msg_text, email_text = make_text()

    # 发送钉钉消息,包含allure报告链接
    logger.info("Sending DingTalk message with the Allure report link...")
    dd_send_msg(msg_text + "\nAllure Report: " + allure_report_link)

    # 发送邮件，包含Allure报告链接
    # send_email(msg_text + "\nAllure Report" + allure_report_link)

    logger.info("Test suite finished.")